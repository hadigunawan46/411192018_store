<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('barang', BarangController::class);
//Route::get('/barang','BarangController@index');
Route::post('/barang/create','BarangController@create');
Route::get('/barang/{id}/edit', 'BarangController@edit');
Route::post('/barang/{id}/update', 'BarangController@update');
Route::get('/barang/{id}/destroy', 'BarangController@destroy');

Route::resource('pelanggan', PelangganController::class);
Route::post('/pelanggan/create','PelangganController@create');
Route::get('/pelanggan/{id}/edit', 'PelangganController@edit');
Route::post('/pelanggan/{id}/update', 'PelangganController@update');
Route::get('/pelanggan/{id}/destroy', 'PelangganController@destroy');

Route::resource('penjualan', PenjualanController::class);
Route::post('/penjualan/create','PenjualanController@create');




