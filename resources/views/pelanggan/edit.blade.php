<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit Data Pelanggan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
    <h1 class="text-center mb-3">Lucky Store</h1>
        <h3 class="mb-3">Edit Data Pelanggan</h3>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        <div class="row">
            <form action="/pelanggan/{{$pelanggan->id}}/update" method="post">
                {{csrf_field()}}
                <div class="mb-3">
                    <label for="kode_pelanggan" class="form-label">Kode Pelanggan</label>
                    <input type="text" name="pelanggan_barang" class="form-control" id="kode_pelanggan" value="{{$pelanggan->kode_pelanggan}}">
                </div>
                <div class="mb-3">
                    <label for="nama_pelanggan" class="form-label">Nama Pelanggan</label>
                    <input type="text" name="nama_pelanggan" class="form-control" id="nama_pelanggan" value="{{$pelanggan->nama_pelanggan}}">
                </div>
                <div class="mb-3">
                    <label for="alamat" class="form-label">Alamat</label>
                    <textarea class="form-control" name="alamat" id="deskripsi" rows="3" >{{$pelanggan->alamat}}</textarea>
                </div>
                <div class="mb-3">
                    <label for="nama_kota" class="form-label">Kota</label>
                    <input type="text" name="nama_kota" class="form-control" id="nama_kota" value="{{$pelanggan->nama_kota}}">
                </div>
                <div class="mb-3">
                    <label for="no_telepon" class="form-label">No Telepon</label>
                    <input type="text" name="no_telepon" class="form-control" id="no_telepon" value="{{$pelanggan->no_telepon}}">
                </div>
                <button type="submit" class="btn btn-warning">Update</button>
            </form>
        </div>
    </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>
</html>