<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Pelanggan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
    <h1 class="text-center mb-5">Lucky Store</h1>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        <div class="row">
            <div class="col-6">
                <h3>Data Pelanggan</h3>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-sm float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Tambah Data Pelanggan
                </button>

            </div>
            <table class="table table-hover table-striped">
                <tr>
                    <th>Kode Pelanggan</th>
                    <th>Nama Pelanggan</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>No Telepon</th>
                    <th>Aksi</th>
                </tr>
                @foreach($data_pelanggan as $pelanggan)
                <tr>
                    <td>{{$pelanggan->kode_pelanggan}}</td>
                    <td>{{$pelanggan->nama_pelanggan}}</td>
                    <td>{{$pelanggan->alamat}}</td>
                    <td>{{$pelanggan->nama_kota}}</td>
                    <td>{{$pelanggan->no_telepon}}</td>
                    <td>
                        <a href="/pelanggan/{{$pelanggan->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <a href="/pelanggan/{{$pelanggan->id}}/destroy" class="btn btn-danger btn-sm">Hapus</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Pelanggan</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/pelanggan/create" method="post">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <label for="kode_pelanggan" class="form-label">Kode Pelanggan</label>
                            <input type="text" name="kode_pelanggan" class="form-control" id="kode_pelanggan">
                        </div>
                        <div class="mb-3">
                            <label for="nama_pelanggan" class="form-label">Nama Pelanggan</label>
                            <input type="text" name="nama_pelanggan" class="form-control" id="nama_pelanggan">
                        </div>
                        <div class="mb-3">
                            <label for="alamat" class="form-label">Alamat</label>
                            <textarea class="form-control" name="alamat" id="deskripsi" rows="3"></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="nama_kota" class="form-label">Kota</label>
                            <input type="text" name="nama_kota" class="form-control" id="nama_kota">
                        </div>
                        <div class="mb-3">
                            <label for="no_telepon" class="form-label">No Telepon</label>
                            <input type="text" name="no_telepon" class="form-control" id="no_telepon">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>