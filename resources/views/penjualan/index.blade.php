<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Penjualan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
    <h1 class="text-center mb-5">Lucky Store</h1>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
            {{session('sukses')}}
        </div>
        @endif
        <div class="row">
            <div class="col-6">
                <h3>Data Penjualan</h3>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-sm float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Tambah Data Penjualan
                </button>

            </div>
            <table class="table table-hover table-striped">
                <tr>
                    <th>No Penjualan</th>
                    <th>Tanggal</th>
                    <th>Kode Pelanggan</th>
                    <th>Kode Barang</th>
                    <th>Jumlah Barang</th>
                    <th>Harga Barang</th>
                </tr>
                @foreach($data_penjualan as $penjualan)
                <tr>
                    <td>{{$penjualan->no_penjualan}}</td>
                    <td>{{$penjualan->tanggal}}</td>
                    <td>{{$penjualan->kode_pelanggan}}</td>
                    <td>{{$penjualan->kode_barang}}</td>
                    <td>{{$penjualan->jumlah_barang}}</td>
                    <td>{{$penjualan->harga_barang}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Penjualan</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/penjualan/create" method="post">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <label for="no_penjualan" class="form-label">No Penjualan</label>
                            <input type="text" name="no_penjualan" class="form-control" id="no_penjualan">
                        </div>
                        <div class="mb-3">
                            <label for="tanggal" class="form-label">Tanggal</label>
                            <input type="date" name="tanggal" class="form-control" id="tanggal">
                        </div>
                        <div class="mb-3">
                            <label for="kode_pelanggan" class="form-label">Kode Pelanggan</label>
                            <input type="text" name="kode_pelanggan" class="form-control" id="kode_pelanggan">
                        </div>
                        <div class="mb-3">
                            <label for="kode_barang" class="form-label">Kode Barang</label>
                            <input type="text" name="kode_barang" class="form-control" id="kode_barang">
                        </div>
                        <div class="mb-3">
                            <label for="jumlah_barang" class="form-label">Jumlah Barang</label>
                            <input type="text" name="jumlah_barang" class="form-control" id="jumlah_barang">
                        </div>
                        <div class="mb-3">
                            <label for="harga_barang" class="form-label">Harga Barang</label>
                            <input type="text" name="harga_barang" class="form-control" id="harga_barang">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>