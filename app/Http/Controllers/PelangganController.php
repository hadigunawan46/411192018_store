<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_pelanggan = \App\Pelanggan::all();
        $data_pelanggan = \App\Pelanggan::where('nama_kota', '=', 'Jakarta')->get();
        return view('pelanggan.index', ['data_pelanggan' => $data_pelanggan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        \App\Pelanggan::create($request->all());
        return redirect('/pelanggan')->with('sukses', 'Data Berhasil ditambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelanggan = \App\Pelanggan::find($id);
        return view('pelanggan/edit',['pelanggan'=> $pelanggan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pelanggan = \App\Pelanggan::find($id);
        $pelanggan->update($request->all());
        return redirect('/pelanggan')->with('sukses', 'Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelanggan = \App\Pelanggan::find($id);
        $pelanggan->delete($pelanggan);
        return redirect('/pelanggan')->with('sukses', 'Data berhasil di hapus');
    }
}
